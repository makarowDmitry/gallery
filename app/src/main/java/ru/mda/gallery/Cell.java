package ru.mda.gallery;

class Cell {
    private String title;
    private String path;

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    String getPath() {
        return path;
    }

    void setPath(String path) {
        this.path = path;
    }
}
