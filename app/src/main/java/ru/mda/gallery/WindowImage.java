package ru.mda.gallery;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;



public class WindowImage extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.open_image);

        String title = getIntent().getStringExtra("TITLE");
        TextView textView = findViewById(R.id.text);
        textView.setText(title);

        String path = getIntent().getStringExtra("PATH");
        ImageView imageView = findViewById(R.id.image);
        imageView.setImageDrawable(Drawable.createFromPath(path));
    }

}

